package org.orders.myorders;

import org.orders.myorders.containers.ChangeStatusInfo;
import org.orders.myorders.containers.OrderCounts;
import org.orders.myorders.containers.OrderDetailsData;
import org.orders.myorders.containers.OrderListData;
import org.orders.myorders.containers.OutputLoginData;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface HourStoreAPI {

    @FormUrlEncoded
    @POST("/api/v1/merchant/login")
    Call<OutputLoginData> login(@Field("username") String username, @Field("password") String password);


    @GET("/api/v1/order/orderCounts")
    Call<OrderCounts> getOrderCounts(@Query("storeId") String id);

    @GET("/api/v1/order/orders/")
    Call<OrderListData> getOrdersList(@Query("storeId") String id,
                                      @Query("orderStatus") String orderCategory,
                                      @Query("size") String orderSize);

    @GET("/api/v1/order/{id}")
    Call<OrderDetailsData> getOrderDetails(@Path("id") String id);

//    @FormUrlEncoded
    @Headers({ "Content-Type: application/json;charset=UTF-8"})
    @PUT("/api/v1/order/{id}/changeStatus")
    Call<Void> changeOrderStatus(@Path("id") String id, @Body ChangeStatusInfo info);

    @Headers("Authorization: Basic {key}")
    @GET("api/v1/epos/orders/acknowledge/{OrderID}")
    Call<Void> printOrderCall(@Path("OrderID") String orderID,
                              @Header("key") String key);
}
