package org.orders.myorders;


public interface OrderListCardClicked {

    void onCardClicked(String id);

}
