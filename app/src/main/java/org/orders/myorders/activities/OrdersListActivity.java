package org.orders.myorders.activities;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.provider.Settings;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;

import org.orders.myorders.HourStoreAPI;
import org.orders.myorders.adapters.OrderListAdapter;
import org.orders.myorders.OrderListCardClicked;
import org.orders.myorders.R;
import org.orders.myorders.utils.RetrofitClient;
import org.orders.myorders.containers.Content;
import org.orders.myorders.containers.OrderListData;
import org.orders.myorders.utils.AlertNetworkDialog;
import org.orders.myorders.utils.Constants;
import org.orders.myorders.utils.InternetState;
import org.orders.myorders.utils.ProjectInfo;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static org.orders.myorders.utils.Constants.ITEM_COUNTS;
import static org.orders.myorders.utils.Constants.MENU_ITEM;
import static org.orders.myorders.utils.InternetState.isConnected;

public class OrdersListActivity extends AppCompatActivity implements OrderListCardClicked {

    private RecyclerView orderListRecycler;
    private OrderListData orderList;
    private List<Content> orderInfoList = new ArrayList<>();
    private TextView noData;
    private String orderCategory;
    private int size;
    private OrderListAdapter recyclerAdapter;
    private Context context;
    private ProgressBar progressBar;


    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

        outState.putString(MENU_ITEM, orderCategory);
        outState.putInt(ITEM_COUNTS, size);
//        outState.putParcelable(Constants.ORDER_LIST_DATA, orderList);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_orders_list);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        if (toolbar != null) {
            getSupportActionBar().setTitle(null);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            toolbar.setNavigationOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    onBackPressed();
                }
            });
        }

        context = this;
        orderListRecycler = (RecyclerView) findViewById(R.id.order_list_recycler);
        progressBar = (ProgressBar) findViewById(R.id.order_list_progress);
        noData = findViewById(R.id.no_data);

        progressBar.setVisibility(View.GONE);
        noData.setVisibility(View.GONE);

        orderListRecycler.setLayoutManager(new LinearLayoutManager(context));

        if (savedInstanceState == null) {
            Bundle extras = getIntent().getExtras();
            if (extras != null) {
                orderCategory = extras.getString(MENU_ITEM);
                size = extras.getInt(ITEM_COUNTS);
                if (orderCategory.equals(Constants.PLACED))
                    saveCurrentPlacedOrders(size);
            }
//        } else {
//            orderCategory = savedInstanceState.getString(MENU_ITEM);
//            size = savedInstanceState.getInt(ITEM_COUNTS);
//            orderList = savedInstanceState.getParcelable(Constants.ORDER_LIST_DATA);
//            if (orderList.getContent() != null && orderList.getContent().size() > 0)
//                orderInfoList = orderList.getContent();
//        }

            if (!isConnected(context) && orderList == null) {
                AlertNetworkDialog.create(context, createPositiveListener(), createNegativeListener());
            } else {
                if (size > 0) {
//                if (orderList == null)
                    getOrderListData(orderCategory, size);
//                else {
//                    orderInfoList = orderList.getContent();
//                    recyclerAdapter = new OrderListAdapter(orderInfoList, context);
//                    orderListRecycler.setAdapter(recyclerAdapter);
                } else {
                    noData.setText(R.string.no_data);
                    noData.setVisibility(View.VISIBLE);
                }

            }

        }
    }

        @Override
        public void onCardClicked (String id){
            Intent intent = new Intent(OrdersListActivity.this, OrderDetailsActivity.class);
            intent.putExtra(Constants.ITEM_ID, id);
            startActivity(intent);
        }

    private void getOrderListData(String orderCategory, int size) {
        progressBar.setVisibility(View.VISIBLE);
        HourStoreAPI apiService = RetrofitClient.getApiService(context);
        Call<OrderListData> orderListDataCall = apiService.getOrdersList(ProjectInfo.readID(this),orderCategory, String.valueOf(size));
        orderListDataCall.enqueue(new Callback<OrderListData>() {
            @Override
            public void onResponse(Call<OrderListData> call, Response<OrderListData> response) {

                orderList = response.body();
                if (orderList != null && orderList.getContent().size() > 0) {
                    orderInfoList = orderList.getContent();
                }
                recyclerAdapter = new OrderListAdapter(orderInfoList, context);
                orderListRecycler.setAdapter(recyclerAdapter);
                progressBar.setVisibility(View.GONE);
            }

            @Override
            public void onFailure(Call<OrderListData> call, Throwable t) {
                orderList = null;
            }
        });
    }

    private DialogInterface.OnClickListener createPositiveListener() {

        final DialogInterface.OnClickListener listener = new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                if (InternetState.isConnected(context)) {
                    getOrderListData(orderCategory, size);
                } else {
                    Intent settingsIntent = new Intent(Settings.ACTION_WIRELESS_SETTINGS);
                    startActivity(settingsIntent);
                    AlertNetworkDialog.create(context, createPositiveListener(), createNegativeListener());
                }
            }
        };
        return listener;
    }

    private DialogInterface.OnClickListener createNegativeListener() {

        DialogInterface.OnClickListener listener = new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                finish();
            }
        };
        return listener;
    }

    private void saveCurrentPlacedOrders(int number) {
        SharedPreferences shredPref = getSharedPreferences(Constants.APP_PREFERENCES_KEY, MODE_PRIVATE);
        shredPref.edit().putString(Constants.CURRENT_PLACED_ORDERS_NUMBER, String.valueOf(number)).apply();
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.settings){
            startActivity(new Intent(this, SettingsActivity.class));
        }
        return super.onOptionsItemSelected(item);
    }
}
