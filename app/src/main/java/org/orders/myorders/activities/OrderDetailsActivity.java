package org.orders.myorders.activities;

import android.Manifest;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.os.Environment;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.getbase.floatingactionbutton.FloatingActionButton;
import com.getbase.floatingactionbutton.FloatingActionsMenu;

import org.orders.myorders.HourStoreAPI;
import org.orders.myorders.adapters.OrderDetailsItemsAdapter;
import org.orders.myorders.R;
import org.orders.myorders.utils.RetrofitClient;
import org.orders.myorders.containers.ChangeStatusInfo;
import org.orders.myorders.containers.OrderDetailsData;
import org.orders.myorders.containers.OrderItem;
import org.orders.myorders.utils.AlertNetworkDialog;
import org.orders.myorders.utils.Constants;
import org.orders.myorders.utils.InternetState;
import org.orders.myorders.utils.ProjectInfo;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static org.orders.myorders.utils.Constants.ITEM_COUNTS;
import static org.orders.myorders.utils.Constants.MENU_ITEM;
import static org.orders.myorders.utils.Constants.PLACED;

public class OrderDetailsActivity extends AppCompatActivity {


    private Context context;
    private String currentItemId;

    private TextView orderId;
    private TextView orderDeliveryType;
    private TextView orderDeliveryPrice;
    private TextView orderPaymentType;
    private TextView orderCompletedBy;
    private TextView orderItemsQuantity;
    private TextView orderTotal;

    private TextView customerName;
    private TextView customerPhone;
    private TextView customerEmail;
    private TextView customerAddress;
    private TextView customerNotes;

    private ProgressBar progressBar;
    private NestedScrollView scrollView;

    private RecyclerView itemsRecycler;
    private OrderDetailsItemsAdapter adapter;

    private OrderDetailsData orderDetailsData;
    private int totalItemsQuantity = 0;

    private FloatingActionsMenu actionsMenu;
    private FloatingActionButton actionAccept;
    private FloatingActionButton actionDecline;

    private SharedPreferences sharedPref;
    private String placedOrdersNumber;

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

        if (orderDetailsData != null) {
            outState.putParcelable(Constants.ORDER_DETAILS_DATA, orderDetailsData);
            outState.putInt(Constants.TOTAL_ITEMS_QUANTITY, totalItemsQuantity);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.details_menu, menu);
        return true;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_order_details);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        sharedPref = getSharedPreferences(Constants.APP_PREFERENCES_KEY, MODE_PRIVATE);

        if (toolbar != null) {
            getSupportActionBar().setTitle(null);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            toolbar.setNavigationOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    onBackPressed();
                }
            });
        }

        scrollView = findViewById(R.id.order_details_scroll);
        scrollView.setVisibility(View.INVISIBLE);

        progressBar = findViewById(R.id.order_list_progress);
        progressBar.setVisibility(View.GONE);

        if (currentItemId == null || currentItemId.equals("")) {
            Bundle data = getIntent().getExtras();
            if (data != null)
                currentItemId = data.getString(Constants.ITEM_ID);
        }

        if (savedInstanceState != null) {
            orderDetailsData = savedInstanceState.getParcelable(Constants.ORDER_DETAILS_DATA);
            totalItemsQuantity = savedInstanceState.getInt(Constants.TOTAL_ITEMS_QUANTITY);
        }

        context = this;

        orderId = findViewById(R.id.order_info_id);
        orderDeliveryType = findViewById(R.id.order_info_delivery_type);
        orderDeliveryPrice = findViewById(R.id.order_info_delivery_price);
        orderPaymentType = findViewById(R.id.order_info_payment_type);
        orderCompletedBy = findViewById(R.id.order_info_completed_by);
        orderItemsQuantity = findViewById(R.id.order_info_items_quantity);
        orderTotal = findViewById(R.id.order_info_total);

        customerName = findViewById(R.id.customer_info_name);
        customerPhone = findViewById(R.id.customer_info_phone);
        customerEmail = findViewById(R.id.customer_info_email);
        customerAddress = findViewById(R.id.customer_info_address);
        customerNotes = findViewById(R.id.customer_info_customer_notes);

        itemsRecycler = findViewById(R.id.items_slide_recycler);
        itemsRecycler.setNestedScrollingEnabled(false);

        actionsMenu = findViewById(R.id.multiple_actions);
        actionAccept = findViewById(R.id.action_accept);
        actionDecline = findViewById(R.id.action_decline);


        placedOrdersNumber = sharedPref.getString(Constants.CURRENT_PLACED_ORDERS_NUMBER, "");


        actionsMenu.setVisibility(View.GONE);
        actionDecline.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                actionsMenu.toggle();
                ChangeStatusInfo info = new ChangeStatusInfo();
                info.setOrderStatus(Constants.REJECTED);
                info.setDeclineReason("rejected");
                changeOrderStatus(currentItemId, info);
            }
        });
        actionAccept.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                actionsMenu.toggle();
                ChangeStatusInfo info = new ChangeStatusInfo();
                info.setOrderStatus(Constants.ACCEPTED);
                info.setDeclineReason("");
                changeOrderStatus(currentItemId, info);
            }
        });

        scrollView.setOnScrollChangeListener(new NestedScrollView.OnScrollChangeListener() {
            @Override
            public void onScrollChange(NestedScrollView v, int scrollX, int scrollY, int oldScrollX, int oldScrollY) {
                if (actionsMenu.getVisibility() != View.GONE) {
                    if (scrollY > oldScrollY)
                        actionsMenu.setVisibility(View.INVISIBLE);
                    if (scrollY < oldScrollY)
                        actionsMenu.setVisibility(View.VISIBLE);
                }}});

        if (!InternetState.isConnected(context) && orderDetailsData == null) {

            AlertNetworkDialog.create(context, createPositiveListener(), createNegativeListener());

        } else {

            if (orderDetailsData == null)
                getOrderDetailsData(currentItemId);
            else {
                scrollView.setVisibility(View.VISIBLE);
                initializeOrderInfoSlide();
                initializeCustomerInfoSlide();
                initializeItemsSlide();
            }
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.print) {
            if (ContextCompat.checkSelfPermission(context, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, Constants.REQUEST_CODE);
            } else
                printData();
        }else if (id == R.id.settings){
            startActivity(new Intent(this, SettingsActivity.class));
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        if (requestCode == Constants.REQUEST_CODE) {
            if (grantResults[0] == RESULT_OK) {
                printData();
            }
        }
    }

    private void getOrderDetailsData(String id) {
        progressBar.setVisibility(View.VISIBLE);
        HourStoreAPI apiService = RetrofitClient.getApiService(this);
        Call<OrderDetailsData> orderDetailsCall = apiService.getOrderDetails(id);
        orderDetailsCall.enqueue(new Callback<OrderDetailsData>() {
            @Override
            public void onResponse(Call<OrderDetailsData> call, Response<OrderDetailsData> response) {
                orderDetailsData = response.body();

                initializeOrderInfoSlide();
                initializeCustomerInfoSlide();
                initializeItemsSlide();

                if (orderDetailsData.getOrderStatus().equals(PLACED))
                    actionsMenu.setVisibility(View.VISIBLE);
                else if (actionsMenu.getVisibility() == View.GONE)
                    actionsMenu.setVisibility(View.GONE);

                progressBar.setVisibility(View.GONE);
                scrollView.setVisibility(View.VISIBLE);

            }

            @Override
            public void onFailure(Call<OrderDetailsData> call, Throwable t) {

            }
        });
    }

    private void initializeOrderInfoSlide() {

        String totalPrice = "£ " + orderDetailsData.getTotalAmount();

        totalItemsQuantity = getTotalItemsQuantity(orderDetailsData.getOrderItems());

        if (orderDetailsData.getId() != 0)
            orderId.setText(String.valueOf(orderDetailsData.getId()));

        if (orderDetailsData.getDelivery() != null && !orderDetailsData.getDelivery().equals(""))
            orderDeliveryType.setText(orderDetailsData.getDelivery());

        if (orderDetailsData.getDeliveryPrice() != null && !orderDetailsData.getDeliveryPrice().equals(""))
            orderDeliveryPrice.setText(orderDetailsData.getDeliveryPrice());

        if (orderDetailsData.getOption() != null && !orderDetailsData.getOption().equals(""))
            orderPaymentType.setText(orderDetailsData.getOption());

        if (orderDetailsData.getWhoPressCompleteButton() != null && !orderDetailsData.getWhoPressCompleteButton().equals(""))
            orderCompletedBy.setText(orderDetailsData.getWhoPressCompleteButton());

        orderItemsQuantity.setText(String.valueOf(totalItemsQuantity));
        orderTotal.setText(totalPrice);

    }

    private void initializeCustomerInfoSlide() {

        if (orderDetailsData.getCustomerName() != null && !orderDetailsData.getCustomerName().equals(""))
            customerName.setText(orderDetailsData.getCustomerName());

        if (orderDetailsData.getCustomerPhone() != null && !orderDetailsData.getCustomerPhone().equals(""))
            customerPhone.setText(orderDetailsData.getCustomerPhone());

        if (orderDetailsData.getCustomerEmail() != null && !orderDetailsData.getCustomerEmail().equals(""))
            customerEmail.setText(orderDetailsData.getCustomerEmail());

        if (orderDetailsData.getCustomerPhone() != null && !orderDetailsData.getCustomerPhone().equals(""))
            customerAddress.setText(orderDetailsData.getCustomerPhone());

        if (orderDetailsData.getDeliveryNotes() != null &&
                !orderDetailsData.getDeliveryNotes().equals("null") &&
                !orderDetailsData.getDeliveryNotes().equals(""))
            customerNotes.setText(orderDetailsData.getDeliveryNotes());

    }

    private void initializeItemsSlide() {
        itemsRecycler.setLayoutManager(new LinearLayoutManager(this));
        adapter = new OrderDetailsItemsAdapter(orderDetailsData.getOrderItems(), this);
        itemsRecycler.setAdapter(adapter);
    }

    private int getTotalItemsQuantity(List<OrderItem> itemList) {
        int count = 0;
        for (OrderItem item : itemList) {
            count += item.getQty();
        }
        return count;
    }

    private DialogInterface.OnClickListener createPositiveListener() {

        final DialogInterface.OnClickListener listener = new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                if (InternetState.isConnected(context)) {
                    getOrderDetailsData(currentItemId);
                } else {
                    Intent settingsIntent = new Intent(Settings.ACTION_WIRELESS_SETTINGS);
                    startActivity(settingsIntent);
                    AlertNetworkDialog.create(context, createPositiveListener(), createNegativeListener());
                }
            }
        };
        return listener;
    }

    private DialogInterface.OnClickListener createNegativeListener() {

        DialogInterface.OnClickListener listener = new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                finish();
            }
        };
        return listener;
    }

    private void printData() {
        if (!isExternalStorageWritable()) {
            Toast.makeText(this, "Can't writeID external storage", Toast.LENGTH_SHORT).show();
            return;
        }
        String totalPrice = "£ " + orderDetailsData.getTotalAmount() + "\n";
        String id = "";
        String price = "";
        String delivery = "";
        String paymentType = "";
        String totalItems = "";
        String completedBy = "";
        totalItemsQuantity = getTotalItemsQuantity(orderDetailsData.getOrderItems());

        if (orderDetailsData.getId() != 0)
            id = "id: " + String.valueOf(orderDetailsData.getId()) + "\n";

        if (orderDetailsData.getDelivery() != null && !orderDetailsData.getDelivery().equals(""))
            delivery = "delivery: " + orderDetailsData.getDelivery() + "\n";

        if (orderDetailsData.getDeliveryPrice() != null && !orderDetailsData.getDeliveryPrice().equals(""))
            price = "price: " + orderDetailsData.getDeliveryPrice() + "\n";

        if (orderDetailsData.getOption() != null && !orderDetailsData.getOption().equals(""))
            paymentType = "payment type: " + orderDetailsData.getOption() + "\n";

        if (orderDetailsData.getWhoPressCompleteButton() != null && !orderDetailsData.getWhoPressCompleteButton().equals(""))
            completedBy = "completed by: " + orderDetailsData.getWhoPressCompleteButton() + "\n";

        File path = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOCUMENTS);
        boolean isCreated = true;
        if (!path.exists()) isCreated = path.mkdir();
        if (!isCreated) {
            Log.d("DetailsActivity", "Can't create folder");
            Toast.makeText(context, "Something went wrong", Toast.LENGTH_SHORT).show();
            return;
        }
        Log.d("DetailsActivity", path.getAbsolutePath());
        File file = new File(path, "output_print_data.txt");
        FileOutputStream fileOutputStream = null;
        try {
            fileOutputStream = new FileOutputStream(file);
            fileOutputStream.write(id.getBytes());
            fileOutputStream.write(delivery.getBytes());
            fileOutputStream.write(price.getBytes());
            fileOutputStream.write(paymentType.getBytes());
            fileOutputStream.write(completedBy.getBytes());
            fileOutputStream.write(totalItems.getBytes());
            fileOutputStream.write(totalPrice.getBytes());

            fileOutputStream.close();

            Toast.makeText(context, "Done!", Toast.LENGTH_SHORT).show();
        } catch (IOException e) {
            e.printStackTrace();
            Toast.makeText(context, "Something went wrong", Toast.LENGTH_SHORT).show();
        }

        HourStoreAPI api = RetrofitClient.getApiService(this);
        Call<Void> call = api.printOrderCall(currentItemId, ProjectInfo.readBasicAuth(context));
        Log.d("PRINT", ProjectInfo.readBasicAuth(context));
        call.enqueue(new Callback<Void>() {
            @Override
            public void onResponse(Call<Void> call, Response<Void> response) {
                Toast.makeText(context, "Call done!", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onFailure(Call<Void> call, Throwable t) {
                Log.e("Print", t.toString());
                Toast.makeText(context, "Something went wrong with call", Toast.LENGTH_SHORT).show();
            }
        });
    }

    public boolean isExternalStorageWritable() {
        String state = Environment.getExternalStorageState();
        return Environment.MEDIA_MOUNTED.equals(state);
    }

    private void changeOrderStatus(String id, final ChangeStatusInfo info){

        progressBar.setVisibility(View.VISIBLE);
        HourStoreAPI apiService = RetrofitClient.getApiService(this);
        Call<Void> orderStatusCall = apiService.changeOrderStatus(id, info);
        orderStatusCall.enqueue(new Callback<Void>() {
            @Override
            public void onResponse(Call<Void> call, Response<Void> response) {
                Toast.makeText(OrderDetailsActivity.this, "Order was " + info.getOrderStatus(), Toast.LENGTH_SHORT).show();

                Intent intent = new Intent(OrderDetailsActivity.this, OrdersListActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                intent.putExtra(MENU_ITEM, PLACED);
                intent.putExtra(ITEM_COUNTS, Integer.valueOf(placedOrdersNumber));
                startActivity(intent);

                OrderDetailsActivity.this.finish();
            }

            @Override
            public void onFailure(Call<Void> call, Throwable t) {

            }
        });
    }

}
