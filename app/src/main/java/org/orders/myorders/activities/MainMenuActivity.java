package org.orders.myorders.activities;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.provider.Settings;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import org.orders.myorders.CheckForNewOrdersService;
import org.orders.myorders.HourStoreAPI;
import org.orders.myorders.R;
import org.orders.myorders.utils.RetrofitClient;
import org.orders.myorders.containers.OrderCounts;
import org.orders.myorders.utils.AlertNetworkDialog;
import org.orders.myorders.utils.Constants;
import org.orders.myorders.utils.InternetState;
import org.orders.myorders.utils.ProjectInfo;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static org.orders.myorders.utils.Constants.ACCEPTED;
import static org.orders.myorders.utils.Constants.APP_PREFERENCES_KEY;
import static org.orders.myorders.utils.Constants.CURRENT_ACCEPTED_ORDERS_COUNT;
import static org.orders.myorders.utils.Constants.CURRENT_DELIVERED_ORDERS_COUNT;
import static org.orders.myorders.utils.Constants.CURRENT_ON_THE_WAY_ORDERS_COUNT;
import static org.orders.myorders.utils.Constants.CURRENT_PLACED_ORDERS_NUMBER;
import static org.orders.myorders.utils.Constants.CURRENT_REJECTED_ORDERS_COUNT;
import static org.orders.myorders.utils.Constants.DELIVERED;
import static org.orders.myorders.utils.Constants.ITEM_COUNTS;
import static org.orders.myorders.utils.Constants.MENU_ITEM;
import static org.orders.myorders.utils.Constants.NEW_ORDERS_FOUND;
import static org.orders.myorders.utils.Constants.ON_THE_WAY;
import static org.orders.myorders.utils.Constants.PLACED;
import static org.orders.myorders.utils.Constants.REJECTED;

public class MainMenuActivity extends AppCompatActivity implements View.OnClickListener {

    private final String TAG = "MainMenuActivity";
    private TextView placed;
    private TextView accepted;
    private TextView onTheWay;
    private TextView rejected;
    private TextView delivered;

    private CardView placedLinear;
    private CardView acceptedLinear;
    private CardView onTheWayLinear;
    private CardView rejectedLinear;
    private CardView deliveredLinear;

    private ProgressBar progressBar;

    private LinearLayout layoutData;
    private SwipeRefreshLayout swipeRefreshLayout;

    private OrderCounts orderCounts;

    private SharedPreferences sharedPref;
    private Context context;

    private boolean firstStart = true;

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main_menu_log_out, menu);
        getMenuInflater().inflate(R.menu.settings, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        if (item.getItemId() == R.id.log_out) {
            sharedPref = getSharedPreferences(Constants.APP_PREFERENCES_KEY, MODE_PRIVATE);
            sharedPref.edit()
                    .putString(Constants.TOKEN, "")
                    .apply();
            startActivity(new Intent(MainMenuActivity.this, LoginActivity.class));
            MainMenuActivity.this.finish();
        } else if (item.getItemId() == R.id.settings){
            startActivity(new Intent(this, SettingsActivity.class));
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (!InternetState.isConnected(context))
            AlertNetworkDialog.create(context, createPositiveListener(), createNegativeListener());

        Log.i(TAG, "--------------ON RESUME MAIN MENU--------------");

        if (!firstStart)
            try{
                getOrderCounts();
            } catch (Exception ex){
                Toast.makeText(this, "Something wrong", Toast.LENGTH_SHORT).show();
            }

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_menu);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        Intent intentService = new Intent(this, CheckForNewOrdersService.class);
        startService(intentService);

        context = this;
        progressBar = (ProgressBar) findViewById(R.id.main_menu_progress);
        progressBar.setVisibility(View.GONE);
        layoutData = (LinearLayout) findViewById(R.id.data_layout);
        layoutData.setVisibility(View.GONE);

        if (toolbar != null) {
            getSupportActionBar().setTitle(null);
        }

        placed = (TextView) findViewById(R.id.placed_text_view);
        accepted = (TextView) findViewById(R.id.accepted_text_view);
        onTheWay = (TextView) findViewById(R.id.on_the_way_text_view);
        rejected = (TextView) findViewById(R.id.rejected_text_view);
        delivered = (TextView) findViewById(R.id.delivered_text_view);

        placedLinear = (CardView) findViewById(R.id.placed_layout);
        acceptedLinear = (CardView) findViewById(R.id.accepted_layout);
        onTheWayLinear = (CardView) findViewById(R.id.on_the_way_layout);
        rejectedLinear = (CardView) findViewById(R.id.rejected_layout);
        deliveredLinear = (CardView) findViewById(R.id.deliveder_layout);

        swipeRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.swipe_main_menu);

        placedLinear.setOnClickListener(this);
        acceptedLinear.setOnClickListener(this);
        onTheWayLinear.setOnClickListener(this);
        rejectedLinear.setOnClickListener(this);
        deliveredLinear.setOnClickListener(this);


        if (!InternetState.isConnected(context)) {
            AlertNetworkDialog.create(context, createPositiveListener(), createNegativeListener());

        } else {
            try{
                getOrderCounts();
            } catch (Exception ex){
                Toast.makeText(this, "Something wrong", Toast.LENGTH_SHORT).show();
            }
        }

        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        if (!InternetState.isConnected(context)) {
                            AlertNetworkDialog.create(context, createPositiveListener(), createNegativeListener());

                        } else {
                            try{
                                getOrderCounts();
                            } catch (Exception ex){
                                Toast.makeText(context, "Something wrong", Toast.LENGTH_SHORT).show();
                            }
                        }
                    }
                }, 1000);
            }
        });

        registerReceiver(newOrdersFound, new IntentFilter(NEW_ORDERS_FOUND));
    }

    private BroadcastReceiver newOrdersFound = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();

            sharedPref = getSharedPreferences(APP_PREFERENCES_KEY, MODE_PRIVATE);
            Log.d(TAG, "-----------------newOrdersFound: " + action + "---------------------");
            if (action.equals(NEW_ORDERS_FOUND)) {
                if (!placed.getText().equals(sharedPref.getString(CURRENT_PLACED_ORDERS_NUMBER, "")))
                    placed.setText(sharedPref.getString(CURRENT_PLACED_ORDERS_NUMBER, ""));

                if (!accepted.getText().equals(sharedPref.getString(CURRENT_ACCEPTED_ORDERS_COUNT, "")))
                    accepted.setText(sharedPref.getString(CURRENT_ACCEPTED_ORDERS_COUNT, ""));

                if (!onTheWay.getText().equals(sharedPref.getString(CURRENT_ON_THE_WAY_ORDERS_COUNT, "")))
                    onTheWay.setText(sharedPref.getString(CURRENT_ON_THE_WAY_ORDERS_COUNT, ""));

                if (!rejected.getText().equals(sharedPref.getString(CURRENT_REJECTED_ORDERS_COUNT, "")))
                    rejected.setText(sharedPref.getString(CURRENT_REJECTED_ORDERS_COUNT, ""));

                if (!delivered.getText().equals(sharedPref.getString(CURRENT_DELIVERED_ORDERS_COUNT, "")))
                    delivered.setText(sharedPref.getString(CURRENT_DELIVERED_ORDERS_COUNT, ""));

            }
        }
    };

    private void getOrderCounts() {
        if (!swipeRefreshLayout.isRefreshing()) progressBar.setVisibility(View.VISIBLE);
        HourStoreAPI apiService = RetrofitClient.getApiService(this);
        Call<OrderCounts> call = apiService.getOrderCounts(ProjectInfo.readID(this));
        call.enqueue(new Callback<OrderCounts>() {
            @Override
            public void onResponse(Call<OrderCounts> call, Response<OrderCounts> response) {
                orderCounts = response.body();
                try{
                    Log.i(TAG, "------------------Connecting from Main Menu------------------");
                    Log.i(TAG + "/resp", response.body().toString());
                    placed.setText(String.valueOf(orderCounts.getPlaced()));
                    accepted.setText(String.valueOf(orderCounts.getAccepted()));
                    onTheWay.setText(String.valueOf(orderCounts.getOnTheWay()));
                    rejected.setText(String.valueOf(orderCounts.getRejected()));
                    delivered.setText(String.valueOf(orderCounts.getDelivered()));

                    progressBar.setVisibility(View.GONE);
                    layoutData.setVisibility(View.VISIBLE);

                    if (swipeRefreshLayout.isRefreshing()) swipeRefreshLayout.setRefreshing(false);

                    SharedPreferences shredPref = getSharedPreferences(Constants.APP_PREFERENCES_KEY, MODE_PRIVATE);
                    shredPref.edit().putString(Constants.CURRENT_PLACED_ORDERS_NUMBER, String.valueOf(orderCounts.getPlaced())).apply();
                }catch (Exception ex){
                    Log.e(TAG, ex.toString());
                }
            }

            @Override
            public void onFailure(Call<OrderCounts> call, Throwable t) {
                Log.i(TAG, "------------------Connecting from Main Menu------------------");

            }
        });

    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.placed_layout: {
                Log.i(TAG + "/click", PLACED);
                Intent intent = new Intent(this, OrdersListActivity.class);
                intent.putExtra(MENU_ITEM, PLACED);
                intent.putExtra(Constants.STARTED_FROM_NOTIF, false);
                if (orderCounts.getPlaced() != 0)
                    intent.putExtra(ITEM_COUNTS, orderCounts.getPlaced());
                else intent.putExtra(ITEM_COUNTS, 0);
                startActivity(intent);
                break;
            }
            case R.id.accepted_layout: {
                Log.i(TAG + "/click", ACCEPTED);
                Intent intent = new Intent(this, OrdersListActivity.class);
                intent.putExtra(MENU_ITEM, ACCEPTED);
                intent.putExtra(Constants.STARTED_FROM_NOTIF, false);
                if (orderCounts.getAccepted() != 0)
                    intent.putExtra(ITEM_COUNTS, orderCounts.getAccepted());
                else intent.putExtra(ITEM_COUNTS, 0);
                startActivity(intent);
                break;
            }
            case R.id.on_the_way_layout: {
                Log.i(TAG + "/click", ON_THE_WAY);
                Intent intent = new Intent(this, OrdersListActivity.class);
                intent.putExtra(MENU_ITEM, ON_THE_WAY);
                intent.putExtra(Constants.STARTED_FROM_NOTIF, false);
                if (orderCounts.getOnTheWay() != 0)
                    intent.putExtra(ITEM_COUNTS, orderCounts.getOnTheWay());
                else intent.putExtra(ITEM_COUNTS, 0);
                startActivity(intent);
                break;
            }
            case R.id.rejected_layout: {
                Log.i(TAG + "/click", REJECTED);
                Intent intent = new Intent(this, OrdersListActivity.class);
                intent.putExtra(MENU_ITEM, REJECTED);
                intent.putExtra(Constants.STARTED_FROM_NOTIF, false);
                if (orderCounts.getRejected() != 0)
                    intent.putExtra(ITEM_COUNTS, orderCounts.getRejected());
                else intent.putExtra(ITEM_COUNTS, 0);
                startActivity(intent);
                break;
            }
            case R.id.deliveder_layout: {
                Log.i(TAG + "/click", DELIVERED);
                Intent intent = new Intent(this, OrdersListActivity.class);
                intent.putExtra(MENU_ITEM, DELIVERED);
                intent.putExtra(Constants.STARTED_FROM_NOTIF, false);
                if (orderCounts.getDelivered() != 0)
                    intent.putExtra(ITEM_COUNTS, orderCounts.getDelivered());
                else intent.putExtra(ITEM_COUNTS, 0);
                startActivity(intent);
            }
        }
    }

    private DialogInterface.OnClickListener createPositiveListener() {

        final DialogInterface.OnClickListener listener = new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                if (InternetState.isConnected(context)) {
                    try{
                        getOrderCounts();
                    } catch (Exception ex){
                        Toast.makeText(context, "Something wrong", Toast.LENGTH_SHORT).show();
                    }
                } else {
                    Intent settingsIntent = new Intent(Settings.ACTION_WIRELESS_SETTINGS);
                    startActivity(settingsIntent);
                    AlertNetworkDialog.create(context, createPositiveListener(), createNegativeListener());
                }
            }
        };
        return listener;
    }

    private DialogInterface.OnClickListener createNegativeListener() {

        DialogInterface.OnClickListener listener = new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                finish();
            }
        };
        return listener;
    }

    @Override
    protected void onPause() {
        super.onPause();
        firstStart = false;
    }

    @Override
    protected void onDestroy() {
        Intent intentService = new Intent(this, CheckForNewOrdersService.class);
        stopService(intentService);
        unregisterReceiver(newOrdersFound);
        super.onDestroy();
    }
}
