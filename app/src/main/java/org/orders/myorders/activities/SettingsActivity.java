package org.orders.myorders.activities;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import org.orders.myorders.R;
import org.orders.myorders.utils.ProjectInfo;

import static org.orders.myorders.utils.Constants.BASIC_AUTH_PREF;
import static org.orders.myorders.utils.Constants.BASIC_ID;

public class SettingsActivity extends AppCompatActivity {

    private EditText id;
    private EditText title;
    private Button save;
    private Button cancel;
    private SharedPreferences sp;

    private Context context;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);

        context = this;

        sp = this.getSharedPreferences(BASIC_AUTH_PREF, MODE_PRIVATE);

        id = (EditText)findViewById(R.id.settings_id);
        id.setText(sp.getString(BASIC_ID, "415"));
        save = (Button)findViewById(R.id.settings_save);
        cancel = (Button)findViewById(R.id.settings_cancel);

        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ProjectInfo.writeID(id.getText().toString(), context);
                finish();
            }
        });

        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
    }
}
