package org.orders.myorders.containers;


import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class OrderListData implements Parcelable {

    @SerializedName("count")
    @Expose
    private int count;
    @SerializedName("content")
    @Expose
    private List<Content> content = null;
    @SerializedName("itemsCount")
    @Expose
    private int itemsCount;

    protected OrderListData(Parcel in) {
        count = in.readInt();
        content = in.createTypedArrayList(Content.CREATOR);
        itemsCount = in.readInt();
    }

    public static final Creator<OrderListData> CREATOR = new Creator<OrderListData>() {
        @Override
        public OrderListData createFromParcel(Parcel in) {
            return new OrderListData(in);
        }

        @Override
        public OrderListData[] newArray(int size) {
            return new OrderListData[size];
        }
    };

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public List<Content> getContent() {
        return content;
    }

    public void setContent(List<Content> content) {
        this.content = content;
    }

    public int getItemsCount() {
        return itemsCount;
    }

    public void setItemsCount(int itemsCount) {
        this.itemsCount = itemsCount;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeInt(count);
        parcel.writeTypedList(content);
        parcel.writeInt(itemsCount);
    }
}
