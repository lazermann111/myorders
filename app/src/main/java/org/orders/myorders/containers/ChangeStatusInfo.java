package org.orders.myorders.containers;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ChangeStatusInfo {

    @SerializedName("orderStatus")
    @Expose
    private String orderStatus;
    @SerializedName("declineReason")
    @Expose
    private String declineReason;

    public String getOrderStatus() {
        return orderStatus;
    }

    public void setOrderStatus(String orderStatus) {
        this.orderStatus = orderStatus;
    }

    public String getDeclineReason() {
        return declineReason;
    }

    public void setDeclineReason(String declineReason) {
        this.declineReason = declineReason;
    }


}
