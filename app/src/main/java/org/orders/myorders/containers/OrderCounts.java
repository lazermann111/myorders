package org.orders.myorders.containers;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class OrderCounts {
    @SerializedName("placed")
    @Expose
    private int placed;
    @SerializedName("rejected")
    @Expose
    private int rejected;
    @SerializedName("on_the_way")
    @Expose
    private int onTheWay;
    @SerializedName("accepted")
    @Expose
    private int accepted;
    @SerializedName("delivered")
    @Expose
    private int delivered;
    @SerializedName("itemsCount")
    @Expose
    private int itemsCount;

    public int getItemsCount() {
        return itemsCount;
    }

    public void setItemsCount(int itemsCount) {
        this.itemsCount = itemsCount;
    }

    public int getPlaced() {
        return placed;
    }

    public void setPlaced(int placed) {
        this.placed = placed;
    }

    public int getRejected() {
        return rejected;
    }

    public void setRejected(int rejected) {
        this.rejected = rejected;
    }

    public int getOnTheWay() {
        return onTheWay;
    }

    public void setOnTheWay(int onTheWay) {
        this.onTheWay = onTheWay;
    }

    public int getAccepted() {
        return accepted;
    }

    public void setAccepted(int accepted) {
        this.accepted = accepted;
    }

    public int getDelivered() {
        return delivered;
    }

    public void setDelivered(int delivered) {
        this.delivered = delivered;
    }
}
