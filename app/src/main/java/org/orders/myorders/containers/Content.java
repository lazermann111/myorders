package org.orders.myorders.containers;


import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Content implements Parcelable{

    @SerializedName("id")
    @Expose
    private int id;
    @SerializedName("delivery")
    @Expose
    private String delivery;
    @SerializedName("created")
    @Expose
    private String created;
    @SerializedName("totalAmount")
    @Expose
    private String totalAmount;


    protected Content(Parcel in) {
        id = in.readInt();
        delivery = in.readString();
        created = in.readString();
        totalAmount = in.readString();
    }

    public static final Creator<Content> CREATOR = new Creator<Content>() {
        @Override
        public Content createFromParcel(Parcel in) {
            return new Content(in);
        }

        @Override
        public Content[] newArray(int size) {
            return new Content[size];
        }
    };

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getDelivery() {
        return delivery;
    }

    public void setDelivery(String delivery) {
        this.delivery = delivery;
    }


    public String getCreated() {
        return created;
    }

    public void setCreated(String created) {
        this.created = created;
    }

    public String getTotalAmount() {
        return totalAmount;
    }

    public void setTotalAmount(String totalAmount) {
        this.totalAmount = totalAmount;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeInt(id);
        parcel.writeString(delivery);
        parcel.writeString(created);
        parcel.writeString(totalAmount);
    }
}
