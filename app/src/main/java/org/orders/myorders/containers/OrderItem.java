package org.orders.myorders.containers;


import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class OrderItem implements Parcelable {


    @SerializedName("id")
    @Expose
    private int id;
    @SerializedName("title")
    @Expose
    private String title;
    @SerializedName("qty")
    @Expose
    private int qty;
    @SerializedName("imageUrl")
    @Expose
    private List<ImageUrl> imageUrl = null;
    @SerializedName("price")
    @Expose
    private String price;
    @SerializedName("totalAmount")
    @Expose
    private String totalAmount;


    protected OrderItem(Parcel in) {
        id = in.readInt();
        title = in.readString();
        qty = in.readInt();
        imageUrl = in.createTypedArrayList(ImageUrl.CREATOR);
        price = in.readString();
        totalAmount = in.readString();
    }

    public static final Creator<OrderItem> CREATOR = new Creator<OrderItem>() {
        @Override
        public OrderItem createFromParcel(Parcel in) {
            return new OrderItem(in);
        }

        @Override
        public OrderItem[] newArray(int size) {
            return new OrderItem[size];
        }
    };

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getQty() {
        return qty;
    }

    public void setQty(int qty) {
        this.qty = qty;
    }


    public List<ImageUrl> getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(List<ImageUrl> imageUrl) {
        this.imageUrl = imageUrl;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }


    public String getTotalAmount() {
        return totalAmount;
    }

    public void setTotalAmount(String totalAmount) {
        this.totalAmount = totalAmount;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeInt(id);
        parcel.writeString(title);
        parcel.writeInt(qty);
        parcel.writeTypedList(imageUrl);
        parcel.writeString(price);
        parcel.writeString(totalAmount);
    }
}
