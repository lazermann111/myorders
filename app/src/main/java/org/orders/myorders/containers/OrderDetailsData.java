package org.orders.myorders.containers;


import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class OrderDetailsData implements Parcelable{

    @SerializedName("id")
    @Expose
    private int id;
    @SerializedName("orderStatus")
    @Expose
    private String orderStatus;
    @SerializedName("deliveryId")
    @Expose
    private int deliveryId;
    @SerializedName("delivery")
    @Expose
    private String delivery;
    @SerializedName("deliveryPrice")
    @Expose
    private String deliveryPrice;
    @SerializedName("orderItems")
    @Expose
    private List<OrderItem> orderItems = null;
    @SerializedName("customerId")
    @Expose
    private int customerId;
    @SerializedName("customerName")
    @Expose
    private String customerName;
    @SerializedName("customerAddress")
    @Expose
    private String customerAddress;
    @SerializedName("completed")
    @Expose
    private String completed;
    @SerializedName("created")
    @Expose
    private String created;
    @SerializedName("deliveryDate")
    @Expose
    private String deliveryDate;
    @SerializedName("customerPhone")
    @Expose
    private String customerPhone;
    @SerializedName("customerEmail")
    @Expose
    private String customerEmail;
    @SerializedName("customerPostcode")
    @Expose
    private String customerPostcode;
    @SerializedName("storeID")
    @Expose
    private int storeID;
    @SerializedName("storeName")
    @Expose
    private String storeName;
    @SerializedName("storeAddress")
    @Expose
    private String storeAddress;
    @SerializedName("storeCity")
    @Expose
    private String storeCity;
    @SerializedName("storePostcode")
    @Expose
    private String storePostcode;
    @SerializedName("storePhone")
    @Expose
    private String storePhone;
    @SerializedName("option")
    @Expose
    private String option;
    @SerializedName("deliveryNotes")
    @Expose
    private String deliveryNotes;
    @SerializedName("amountWithoutDelivery")
    @Expose
    private String amountWithoutDelivery;
    @SerializedName("fullAmount")
    @Expose
    private String fullAmount;
    @SerializedName("totalAmount")
    @Expose
    private String totalAmount;
    @SerializedName("earningAmount")
    @Expose
    private String earningAmount;
    @SerializedName("totalCreditCompensationAmount")
    @Expose
    private int totalCreditCompensationAmount;
    @SerializedName("whoPressCompleteButton")
    @Expose
    private String whoPressCompleteButton;
    @SerializedName("storeMinimumOrderValue")
    @Expose
    private String storeMinimumOrderValue;
    @SerializedName("amountWithoutDeliveryAndExcluded")
    @Expose
    private String amountWithoutDeliveryAndExcluded;
    @SerializedName("fullAmountWithoutExcluded")
    @Expose
    private String fullAmountWithoutExcluded;
    @SerializedName("totalAmountWithoutExcluded")
    @Expose
    private String totalAmountWithoutExcluded;

    protected OrderDetailsData(Parcel in) {
        id = in.readInt();
        orderStatus = in.readString();
        deliveryId = in.readInt();
        delivery = in.readString();
        deliveryPrice = in.readString();
        customerId = in.readInt();
        customerName = in.readString();
        customerAddress = in.readString();
        completed = in.readString();
        created = in.readString();
        deliveryDate = in.readString();
        customerPhone = in.readString();
        customerEmail = in.readString();
        customerPostcode = in.readString();
        storeID = in.readInt();
        storeName = in.readString();
        storeAddress = in.readString();
        storeCity = in.readString();
        storePostcode = in.readString();
        storePhone = in.readString();
        option = in.readString();
        deliveryNotes = in.readString();
        amountWithoutDelivery = in.readString();
        fullAmount = in.readString();
        totalAmount = in.readString();
        earningAmount = in.readString();
        totalCreditCompensationAmount = in.readInt();
        whoPressCompleteButton = in.readString();
        storeMinimumOrderValue = in.readString();
        amountWithoutDeliveryAndExcluded = in.readString();
        fullAmountWithoutExcluded = in.readString();
        totalAmountWithoutExcluded = in.readString();
    }

    public static final Creator<OrderDetailsData> CREATOR = new Creator<OrderDetailsData>() {
        @Override
        public OrderDetailsData createFromParcel(Parcel in) {
            return new OrderDetailsData(in);
        }

        @Override
        public OrderDetailsData[] newArray(int size) {
            return new OrderDetailsData[size];
        }
    };

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getOrderStatus() {
        return orderStatus;
    }

    public void setOrderStatus(String orderStatus) {
        this.orderStatus = orderStatus;
    }

    public int getDeliveryId() {
        return deliveryId;
    }

    public void setDeliveryId(int deliveryId) {
        this.deliveryId = deliveryId;
    }

    public String getDelivery() {
        return delivery;
    }

    public void setDelivery(String delivery) {
        this.delivery = delivery;
    }

    public String getDeliveryPrice() {
        return deliveryPrice;
    }

    public void setDeliveryPrice(String deliveryPrice) {
        this.deliveryPrice = deliveryPrice;
    }

    public List<OrderItem> getOrderItems() {
        return orderItems;
    }

    public void setOrderItems(List<OrderItem> orderItems) {
        this.orderItems = orderItems;
    }

    public int getCustomerId() {
        return customerId;
    }

    public void setCustomerId(int customerId) {
        this.customerId = customerId;
    }

    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    public String getCustomerAddress() {
        return customerAddress;
    }

    public void setCustomerAddress(String customerAddress) {
        this.customerAddress = customerAddress;
    }

    public String getCompleted() {
        return completed;
    }

    public void setCompleted(String completed) {
        this.completed = completed;
    }

    public String getCreated() {
        return created;
    }

    public void setCreated(String created) {
        this.created = created;
    }

    public String getDeliveryDate() {
        return deliveryDate;
    }

    public void setDeliveryDate(String deliveryDate) {
        this.deliveryDate = deliveryDate;
    }

    public String getCustomerPhone() {
        return customerPhone;
    }

    public void setCustomerPhone(String customerPhone) {
        this.customerPhone = customerPhone;
    }

    public String getCustomerEmail() {
        return customerEmail;
    }

    public void setCustomerEmail(String customerEmail) {
        this.customerEmail = customerEmail;
    }

    public String getCustomerPostcode() {
        return customerPostcode;
    }

    public void setCustomerPostcode(String customerPostcode) {
        this.customerPostcode = customerPostcode;
    }

    public int getStoreID() {
        return storeID;
    }

    public void setStoreID(int storeID) {
        this.storeID = storeID;
    }

    public String getStoreName() {
        return storeName;
    }

    public void setStoreName(String storeName) {
        this.storeName = storeName;
    }

    public String getStoreAddress() {
        return storeAddress;
    }

    public void setStoreAddress(String storeAddress) {
        this.storeAddress = storeAddress;
    }

    public String getStoreCity() {
        return storeCity;
    }

    public void setStoreCity(String storeCity) {
        this.storeCity = storeCity;
    }

    public String getStorePostcode() {
        return storePostcode;
    }

    public void setStorePostcode(String storePostcode) {
        this.storePostcode = storePostcode;
    }

    public String getStorePhone() {
        return storePhone;
    }

    public void setStorePhone(String storePhone) {
        this.storePhone = storePhone;
    }

    public String getOption() {
        return option;
    }

    public void setOption(String option) {
        this.option = option;
    }

    public String getDeliveryNotes() {
        return deliveryNotes;
    }

    public void setDeliveryNotes(String deliveryNotes) {
        this.deliveryNotes = deliveryNotes;
    }

    public String getAmountWithoutDelivery() {
        return amountWithoutDelivery;
    }

    public void setAmountWithoutDelivery(String amountWithoutDelivery) {
        this.amountWithoutDelivery = amountWithoutDelivery;
    }

    public String getFullAmount() {
        return fullAmount;
    }

    public void setFullAmount(String fullAmount) {
        this.fullAmount = fullAmount;
    }

    public String getTotalAmount() {
        return totalAmount;
    }

    public void setTotalAmount(String totalAmount) {
        this.totalAmount = totalAmount;
    }

    public String getEarningAmount() {
        return earningAmount;
    }

    public void setEarningAmount(String earningAmount) {
        this.earningAmount = earningAmount;
    }

    public int getTotalCreditCompensationAmount() {
        return totalCreditCompensationAmount;
    }

    public void setTotalCreditCompensationAmount(int totalCreditCompensationAmount) {
        this.totalCreditCompensationAmount = totalCreditCompensationAmount;
    }

    public String getWhoPressCompleteButton() {
        return whoPressCompleteButton;
    }

    public void setWhoPressCompleteButton(String whoPressCompleteButton) {
        this.whoPressCompleteButton = whoPressCompleteButton;
    }

    public String getStoreMinimumOrderValue() {
        return storeMinimumOrderValue;
    }

    public void setStoreMinimumOrderValue(String storeMinimumOrderValue) {
        this.storeMinimumOrderValue = storeMinimumOrderValue;
    }

    public String getAmountWithoutDeliveryAndExcluded() {
        return amountWithoutDeliveryAndExcluded;
    }

    public void setAmountWithoutDeliveryAndExcluded(String amountWithoutDeliveryAndExcluded) {
        this.amountWithoutDeliveryAndExcluded = amountWithoutDeliveryAndExcluded;
    }

    public String getFullAmountWithoutExcluded() {
        return fullAmountWithoutExcluded;
    }

    public void setFullAmountWithoutExcluded(String fullAmountWithoutExcluded) {
        this.fullAmountWithoutExcluded = fullAmountWithoutExcluded;
    }

    public String getTotalAmountWithoutExcluded() {
        return totalAmountWithoutExcluded;
    }

    public void setTotalAmountWithoutExcluded(String totalAmountWithoutExcluded) {
        this.totalAmountWithoutExcluded = totalAmountWithoutExcluded;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeInt(id);
        parcel.writeString(orderStatus);
        parcel.writeInt(deliveryId);
        parcel.writeString(delivery);
        parcel.writeString(deliveryPrice);
        parcel.writeInt(customerId);
        parcel.writeString(customerName);
        parcel.writeString(customerAddress);
        parcel.writeString(completed);
        parcel.writeString(created);
        parcel.writeString(deliveryDate);
        parcel.writeString(customerPhone);
        parcel.writeString(customerEmail);
        parcel.writeString(customerPostcode);
        parcel.writeInt(storeID);
        parcel.writeString(storeName);
        parcel.writeString(storeAddress);
        parcel.writeString(storeCity);
        parcel.writeString(storePostcode);
        parcel.writeString(storePhone);
        parcel.writeString(option);
        parcel.writeString(deliveryNotes);
        parcel.writeString(amountWithoutDelivery);
        parcel.writeString(fullAmount);
        parcel.writeString(totalAmount);
        parcel.writeString(earningAmount);
        parcel.writeInt(totalCreditCompensationAmount);
        parcel.writeString(whoPressCompleteButton);
        parcel.writeString(storeMinimumOrderValue);
        parcel.writeString(amountWithoutDeliveryAndExcluded);
        parcel.writeString(fullAmountWithoutExcluded);
        parcel.writeString(totalAmountWithoutExcluded);
    }
}
