package org.orders.myorders.utils;

import android.content.Context;
import android.content.SharedPreferences;

import static android.content.Context.MODE_PRIVATE;
import static org.orders.myorders.utils.Constants.BASIC_AUTH;
import static org.orders.myorders.utils.Constants.BASIC_AUTH_PREF;
import static org.orders.myorders.utils.Constants.BASIC_ID;

public class ProjectInfo {

    public static void writeID(String id, Context context){
        SharedPreferences shared = context.getSharedPreferences(BASIC_AUTH_PREF, MODE_PRIVATE);
        SharedPreferences.Editor editor = shared.edit();
        editor.putString(BASIC_ID, id);
        editor.apply();
    }

    public static void writeKey(String key, Context context){
        SharedPreferences sharedPreferences = context.getSharedPreferences(BASIC_AUTH_PREF, MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(BASIC_AUTH, key);
        editor.apply();
    }

    public static String readID(Context context){
        SharedPreferences shared = context.getSharedPreferences(BASIC_AUTH_PREF, MODE_PRIVATE);
        return shared.getString(Constants.BASIC_ID, "415");
    }

    public static String readBasicAuth(Context context){
        SharedPreferences shared = context.getSharedPreferences(BASIC_AUTH_PREF, MODE_PRIVATE);
        return shared.getString(Constants.BASIC_AUTH, "");
    }
}
