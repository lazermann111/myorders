package org.orders.myorders.utils;

import android.util.Log;

import java.io.File;

public class FindFile {
    public static boolean isFileFound(String path, String fileName) {
        File[] files = new File(path).listFiles();

        Log.d("Files", String.valueOf(files.length));
        if (files.length > 0){
            for (File file : files){
                if (file.isFile()){
                    Log.d("FILENAME", file.getName());
                    if (file.getName().equals(fileName)) return true;
                } else if (file.isDirectory()){
                    isFileFound(file.getPath(), fileName);
                }
            }
        }
        return false;
    }
}
