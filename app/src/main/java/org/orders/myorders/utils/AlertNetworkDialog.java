package org.orders.myorders.utils;


import android.content.Context;
import android.content.DialogInterface;
import android.support.v7.app.AlertDialog;

public class AlertNetworkDialog {

    public static void create (final Context context, DialogInterface.OnClickListener positiveListener,
                               DialogInterface.OnClickListener negativeInterface){

        final AlertDialog alertDialog = new AlertDialog.Builder(context).create();
        alertDialog.setMessage("Internet connection not available!");
        alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, "Retry", positiveListener);
        alertDialog.setButton(AlertDialog.BUTTON_NEGATIVE, "Cancel", negativeInterface);
        alertDialog.show();
    }

}
