package org.orders.myorders.utils;


import android.content.Context;
import android.content.SharedPreferences;
import android.support.annotation.NonNull;

import org.orders.myorders.HourStoreAPI;

import java.io.IOException;

import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class RetrofitClient {

    private static String token;

    private static Interceptor mTokenInterceptor = new Interceptor() {
        @Override
        public Response intercept(@NonNull Chain chain) throws IOException {
            Request request = chain.request();
            if (token != null && !token.equals("")) {
                Request.Builder requestBuilder = request.newBuilder()
                        .addHeader("x-auth-token", token);
                Request newRequest = requestBuilder.build();

                return chain.proceed(newRequest);
            }
            return chain.proceed(request);

        }
    };

    private static OkHttpClient client = new OkHttpClient.Builder()
            .addNetworkInterceptor(mTokenInterceptor)
            .build();


    private static Retrofit getRetrofitInstance() {
        return new Retrofit.Builder()
                .client(client)
                .baseUrl(Constants.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
    }


    public static HourStoreAPI getApiService(Context context) {
        SharedPreferences sharedPref = context.getSharedPreferences(Constants.APP_PREFERENCES_KEY, Context.MODE_PRIVATE);
        String userToken = sharedPref.getString(Constants.TOKEN, "");

        if(!userToken.equals("")){
            token = userToken;
        }

        return getRetrofitInstance().create(HourStoreAPI.class);
    }

}
