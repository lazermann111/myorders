package org.orders.myorders.utils;



public class Constants {
    public static final String BASE_URL = "https://stg.hourstore.co.uk";

    //for SharedPreferences
    public static final String TOKEN = "token";
    public static final String APP_PREFERENCES_KEY = "app pref key";
    public static final String CURRENT_PLACED_ORDERS_NUMBER = "current placed orders number";
    public static final String CURRENT_REJECTED_ORDERS_COUNT = "current REJECTED oreder count";
    public static final String CURRENT_DELIVERED_ORDERS_COUNT = "current DELIVERED oreder count";
    public static final String CURRENT_ACCEPTED_ORDERS_COUNT = "current ACCEPTED oreder count";
    public static final String CURRENT_ON_THE_WAY_ORDERS_COUNT = "current ON_THE_WAY oreder count";

    public static final String BASIC_AUTH = "Basic auth";
    public static final String BASIC_ID = "basic id";
    public static final String BASIC_AUTH_PREF = "Basic auth pref";

    public static final String NEW_ORDERS_FOUND = "org.orders.myorders.NEW_ORDERS_FOUND";
    // min one cycle
    public static final long NOTIFY_INTERVAL = 2 * 60 * 1000;

    //for putExtra
    public static final String MENU_ITEM = "menu item";
    public static final String ITEM_COUNTS = "item counts";
    public static final String PLACED = "PLACED";
    public static final String ON_THE_WAY = "ON_THE_WAY";
    public static final String ACCEPTED = "ACCEPTED";
    public static final String REJECTED = "REJECTED";
    public static final String DELIVERED = "DELIVERED";
    public static final String ITEM_ID = "item id";
    public static final String ORDER_LIST_DATA = "order list data";
    public static final String ORDER_DETAILS_DATA = "order details data";
    public static final String TOTAL_ITEMS_QUANTITY = "total items quantity";
    public static final String STARTED_FROM_NOTIF = "started from notification";
    public static final String ITEMS_NUMBER = "items number";

    //For Runtime Permissions
    public static int REQUEST_CODE = 101;
}
