package org.orders.myorders;

import android.app.Application;

import org.orders.myorders.utils.Constants;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class MyOrdersApplication extends Application{
    private static MyOrdersApplication myOrdersApplication;
    private Retrofit client;
    @Override
    public void onCreate() {
        super.onCreate();
        myOrdersApplication = this;
    }

    public static synchronized MyOrdersApplication getMyOrdersApplication(){
        return myOrdersApplication;
    }
    public Retrofit getClient(){
        if(client != null)
            client = new Retrofit.Builder()
                    .baseUrl(Constants.BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
        return client;
    }
}
