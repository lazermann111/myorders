package org.orders.myorders;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Handler;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import org.orders.myorders.containers.OrderCounts;
import org.orders.myorders.utils.Constants;
import org.orders.myorders.utils.ProjectInfo;
import org.orders.myorders.utils.RetrofitClient;

import java.util.Timer;
import java.util.TimerTask;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static org.orders.myorders.utils.Constants.ITEM_COUNTS;
import static org.orders.myorders.utils.Constants.NEW_ORDERS_FOUND;
import static org.orders.myorders.utils.Constants.NOTIFY_INTERVAL;

public class CheckForNewOrdersService extends Service {

    private static final String TAG = "CheckForNewOrders";

    // run on another Thread to avoid crash
    private Handler mHandler = new Handler();

    private Timer mTimer = new Timer();
    private boolean hasStarted = false;

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onCreate() {
        super.onCreate();
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Log.i(TAG, "----------------------Service was started----------------------");

        if (!hasStarted)
            mTimer.schedule(new MainLogic(), 0, NOTIFY_INTERVAL);

        return START_REDELIVER_INTENT;
    }

    @Override
    public void onDestroy() {
        Log.i(TAG, "----------------------Service was stopped----------------------");
        super.onDestroy();
    }


    private class MainLogic extends TimerTask {

        @Override
        public void run() {
            hasStarted = true;
            mHandler.post(new Runnable() {
                @Override
                public void run() {
                    try{
                        checkForNewOrdersCall();
                    } catch (Exception ex){
                        Log.e(TAG, ex.toString());
                    }

                }
            });
        }
    }

    private void checkForNewOrdersCall() {

        final SharedPreferences sharedPref = getSharedPreferences(Constants.APP_PREFERENCES_KEY, Context.MODE_PRIVATE);

        HourStoreAPI apiService = RetrofitClient.getApiService(this);
        Call<OrderCounts> call = apiService.getOrderCounts(ProjectInfo.readID(this));
        call.enqueue(new Callback<OrderCounts>() {
            @Override
            public void onResponse(Call<OrderCounts> call, Response<OrderCounts> response) {

                Log.i(TAG, "------------------Connecting from Service------------------");

                OrderCounts orderCounts = response.body();
                try{
                    sharedPref.edit().putString(Constants.CURRENT_PLACED_ORDERS_NUMBER, String.valueOf(orderCounts.getPlaced())).apply();
                    sharedPref.edit().putString(Constants.CURRENT_REJECTED_ORDERS_COUNT, String.valueOf(orderCounts.getRejected())).apply();
                    sharedPref.edit().putString(Constants.CURRENT_DELIVERED_ORDERS_COUNT, String.valueOf(orderCounts.getDelivered())).apply();
                    sharedPref.edit().putString(Constants.CURRENT_ACCEPTED_ORDERS_COUNT, String.valueOf(orderCounts.getAccepted())).apply();
                    sharedPref.edit().putString(Constants.CURRENT_ON_THE_WAY_ORDERS_COUNT, String.valueOf(orderCounts.getOnTheWay())).apply();
                    sharedPref.edit().putInt(ITEM_COUNTS, orderCounts.getItemsCount()).apply();
                } catch (Exception ex){
                    Log.e(TAG, ex.toString());
                }

                Intent i = new Intent();
                i.setAction(NEW_ORDERS_FOUND);
                sendBroadcast(i);
                try{
                    if (orderCounts.getPlaced() > 0) {
                        showNotification(orderCounts.getPlaced());
                    }
                } catch (Exception ex){
                    Log.e(TAG, ex.toString());
                }
            }

            @Override
            public void onFailure(Call<OrderCounts> call, Throwable t) {
                Log.i(TAG, "------------------Connecting from Service------------------");
            }
        });

    }

    private void showNotification(int ordersNumber) {

        NotificationManager notifManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        NotificationCompat.InboxStyle inboxStyle = new NotificationCompat.InboxStyle();
        String title;

        if (ordersNumber == 1)
            title = "You have " + String.valueOf(ordersNumber) + " placed order!";
        else
            title = "You have " + String.valueOf(ordersNumber) + " placed orders!";


        NotificationCompat.Builder notifBuilder = new NotificationCompat.Builder(this)
                .setSmallIcon(R.mipmap.ic_launcher)
                .setContentTitle(title)
                .setContentText("")
                .setStyle(inboxStyle)
                .setDefaults(Notification.DEFAULT_VIBRATE | Notification.DEFAULT_SOUND);

        notifManager.notify(671, notifBuilder.build());
    }

}
