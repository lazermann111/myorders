package org.orders.myorders.adapters;

import android.content.Context;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import org.orders.myorders.OrderListCardClicked;
import org.orders.myorders.R;
import org.orders.myorders.containers.Content;
import org.orders.myorders.utils.DateFormatter;

import java.util.ArrayList;
import java.util.List;


public class OrderListAdapter extends RecyclerView.Adapter<OrderListAdapter.OrderListHolder> {

    private List<Content> orderInfoList = new ArrayList<>();
    private Context context;
    private OrderListCardClicked cardClicked;

    public OrderListAdapter(List<Content> orderInfoList, Context context) {
        this.orderInfoList = orderInfoList;
        this.context = context;
        cardClicked = (OrderListCardClicked) context;
    }

    @Override
    public OrderListHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.order_list_item, parent, false);
        return new OrderListHolder(view);
    }

    @Override
    public void onBindViewHolder(OrderListHolder holder, int position) {
        holder.bindView(orderInfoList.get(position), position);
        final String id = String.valueOf(orderInfoList.get(position).getId());
        holder.cardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                cardClicked.onCardClicked(id);
            }
        });
    }

    @Override
    public int getItemCount() {
        return orderInfoList.size();
    }

    class OrderListHolder extends RecyclerView.ViewHolder {
        private TextView orderNumber;
        private TextView orderId;
        private TextView orderDelivery;
        private TextView orderDate;
        private TextView orderPrice;
        private CardView cardView;

        public OrderListHolder(View itemView) {
            super(itemView);

            orderNumber = (TextView) itemView.findViewById(R.id.order_number);
            orderId = (TextView) itemView.findViewById(R.id.order_id);
            orderDelivery = (TextView) itemView.findViewById(R.id.order_delivery);
            orderDate = (TextView) itemView.findViewById(R.id.order_date);
            orderPrice = (TextView) itemView.findViewById(R.id.order_price);
            cardView = (CardView) itemView.findViewById(R.id.order_list_item_card);
        }

        public void bindView(Content order, int position){
            String price = "£ " + order.getTotalAmount();
            String date = DateFormatter.changeFormat(Long.valueOf(order.getCreated()));

            orderNumber.setText(String.valueOf(position + 1));
            orderId.setText(String.valueOf(order.getId()));
            orderDelivery.setText(order.getDelivery());
            orderDate.setText(date);
            orderPrice.setText(price);

        }
    }

}
