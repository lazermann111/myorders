package org.orders.myorders.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import org.orders.myorders.R;
import org.orders.myorders.containers.OrderItem;

import java.util.ArrayList;
import java.util.List;


public class OrderDetailsItemsAdapter extends RecyclerView.Adapter<OrderDetailsItemsAdapter.OrderDetailsItemsHolder> {

    private List<OrderItem> orderItemList = new ArrayList<>();
    private Context context;

    public OrderDetailsItemsAdapter(List<OrderItem> orderItemList, Context context) {
        this.orderItemList = orderItemList;
        this.context = context;
    }

    @Override
    public OrderDetailsItemsHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.items_slide_item, parent, false);
        return new OrderDetailsItemsHolder(view);
    }

    @Override
    public void onBindViewHolder(OrderDetailsItemsHolder holder, int position) {

        holder.bindView(orderItemList.get(position), context);

    }

    @Override
    public int getItemCount() {
        return orderItemList.size();
    }

    class OrderDetailsItemsHolder extends RecyclerView.ViewHolder {

        private ImageView itemImage;
        private TextView itemName;
        private TextView itemQuantity;
        private TextView itemPrice;
        private TextView itemSum;

        public OrderDetailsItemsHolder(View itemView) {
            super(itemView);

            itemImage = itemView.findViewById(R.id.item_slide_image);
            itemName = itemView.findViewById(R.id.item_slide_name);
            itemQuantity = itemView.findViewById(R.id.item_slide_quantity);
            itemPrice = itemView.findViewById(R.id.item_slide_price);
            itemSum = itemView.findViewById(R.id.item_slide_sum);
        }

        public void bindView(OrderItem orderItem, Context context) {

            String price = "£ " + orderItem.getPrice();
            String sum = "£ " + orderItem.getTotalAmount();

            itemName.setText(orderItem.getTitle());
            itemQuantity.setText(String.valueOf(orderItem.getQty()));
            itemPrice.setText(price);
            itemSum.setText(sum);

            if (orderItem.getImageUrl() != null && orderItem.getImageUrl().size() > 0){
                String imgUrl = orderItem.getImageUrl().get(0).getUrl();
                Picasso.with(context)
                        .load(imgUrl)
                        .placeholder(R.mipmap.ic_launcher)
                        .error(R.mipmap.ic_logo)
                        .into(itemImage);
            }

        }
    }

}
